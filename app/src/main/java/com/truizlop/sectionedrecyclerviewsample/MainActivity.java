/*
 * Copyright (C) 2015 Tomás Ruiz-López.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.truizlop.sectionedrecyclerviewsample;


import com.truizlop.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.truizlop.sectionedrecyclerviewsample.viewholders.AgendaSimpleSectionAdapter;
import com.truizlop.sectionedrecyclerviewsample.viewholders.CountSectionAdapter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TextField;

import java.util.List;

public class MainActivity extends Ability {

    private ListContainer recycler;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_main);
        recycler = (ListContainer) findComponentById(ResourceTable.Id_recycler);
        setupRecycler();
    }

    protected void setupRecycler() {
        findComponentById(ResourceTable.Id_refresh).setClickedListener(component -> {
            TextField textFieldH = (TextField) findComponentById(ResourceTable.Id_edH);
            TextField textFieldL = (TextField) findComponentById(ResourceTable.Id_edL);

            int h = Integer.parseInt(textFieldH.getText().isEmpty() ? "5" : textFieldH.getText());
            int l = Integer.parseInt(textFieldL.getText().isEmpty() ? "2" : textFieldL.getText());

            CountSectionAdapter myCountSectionAdapter = new CountSectionAdapter(h, l, MainActivity.this);
            recycler.setItemProvider(myCountSectionAdapter);
        });

        findComponentById(ResourceTable.Id_nomalText).setClickedListener(component -> {
            List<String> arryData = SectionedRecyclerViewAdapter.getArryData(2);
            AgendaSimpleSectionAdapter adapter = new AgendaSimpleSectionAdapter(arryData, MainActivity.this);
            recycler.setItemProvider(adapter);
        });
    }

}
