/*
 * Copyright (C) 2015 Tomás Ruiz-López.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.truizlop.sectionedrecyclerviewsample.viewholders;


import com.truizlop.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.truizlop.sectionedrecyclerviewsample.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

public class CountSectionAdapter extends SectionedRecyclerViewAdapter<CountHeaderViewHolder,
        CountItemViewHolder,
        CountFooterViewHolder> {

    public CountSectionAdapter(int section, int columnCount, Context context) {
        super(getArryData(section), columnCount, context);
    }

    public CountSectionAdapter(int section, Context context) {
        super(getArryData(section), context);
    }

    @Override
    protected int getItemCountForSection(int section) {
        return section + 1;
    }


    @Override
    protected boolean hasFooterInSection(int section) {
        return true;
    }


    protected LayoutScatter getLayoutInflater() {
        return LayoutScatter.getInstance(context);
    }

    @Override
    protected CountHeaderViewHolder onCreateSectionHeaderViewHolder(ComponentContainer parent, int viewType) {
        Component view = getLayoutInflater().parse(ResourceTable.Layout_view_count_header, parent, false);
        return new CountHeaderViewHolder(view);
    }

    @Override
    protected CountFooterViewHolder onCreateSectionFooterViewHolder(ComponentContainer parent, int viewType) {
        Component view = getLayoutInflater().parse(ResourceTable.Layout_view_count_footer, parent, false);
        return new CountFooterViewHolder(view);
    }

    @Override
    protected CountItemViewHolder onCreateItemViewHolder(ComponentContainer parent, int viewType) {
        Component view = getLayoutInflater().parse(ResourceTable.Layout_view_count_item, parent, false);
        return new CountItemViewHolder(view);
    }


    @Override
    protected void onBindSectionHeaderViewHolder(CountHeaderViewHolder holder, int section) {
        holder.render("Section " + (section + 1));
    }

    @Override
    protected void onBindSectionFooterViewHolder(CountFooterViewHolder holder, int section) {
        holder.render("Footer " + (section + 1));
    }

    protected int[] colors = new int[]{0xfff44336, 0xff2196f3, 0xff009688, 0xff8bc34a, 0xffff9800};

    @Override
    protected void onBindItemViewHolder(CountItemViewHolder holder, int section, int position) {
        int color;
        int length = colors.length;
        if (section < length) {color = colors[section];}
        else {color = colors[section % length];}
        ShapeElement element = new ShapeElement();
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        element.setRgbColor(rgbColor);

        try {
            holder.render(String.valueOf(position + 1), context.getResourceManager().getElement(ResourceTable.Color_black).getColor());
            holder.baseComponent.setBackground(element);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
