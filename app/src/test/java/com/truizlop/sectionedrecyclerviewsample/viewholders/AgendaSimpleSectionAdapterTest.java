package com.truizlop.sectionedrecyclerviewsample.viewholders;

import com.truizlop.sectionedrecyclerviewsample.App;
import junit.framework.TestCase;
import org.junit.Assert;

public class AgendaSimpleSectionAdapterTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
        App app = App.getApp();
    }

    public void tearDown() throws Exception {
    }

    public void testGetSectionHeaderTitle() {
        String sectionHeaderTitle = getSectionHeaderTitle1(1);
        Assert.assertSame(sectionHeaderTitle, "Tomorrow");
    }

    public void testGetItemCountForSection() {
        int itemCountForSection = getItemCountForSection1(3);
        Assert.assertSame(3, itemCountForSection);
    }


    //copy by AgendaSimpleSectionAdapter.class
    protected String getSectionHeaderTitle1(int section) {
        return section == 0 ? "Today" : "Tomorrow";
    }

    protected int getItemCountForSection1(int section) {
        return 3;
    }
}