/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.truizlop.sectionedrecyclerview;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.util.List;

class SectionItemProvider extends BaseItemProvider {


    private SetItemViewI setItemViewI;
    private final List<?> mListData;

    SectionItemProvider(List<?> listData) {
        this.mListData = listData;
    }


    SectionItemProvider(List<String> listData, SetItemViewI setItemViewI) {
        this.setItemViewI = setItemViewI;
        this.mListData = listData;
    }


    @Override
    public int getCount() {
        return mListData == null ? 0 : mListData.size();
    }

    @Override
    public Object getItem(int position) {
        if (mListData != null && position >= 0 && position < mListData.size()) {
            return mListData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        return setItemViewI.updateItemView(i, component, componentContainer);
    }


    public interface SetItemViewI {
        Component updateItemView(int position, Component convertComponent, ComponentContainer componentContainer);
    }
}
