/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.truizlop.sectionedrecyclerview;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.List;

public abstract class BaseListProvider<T, V extends BaseViewHolder> extends BaseItemProvider {
    private List<T> listData;
    public Context context;

    public BaseListProvider(List<T> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }


    public void setList(List<T> list) {
        listData = list;
    }

    @Override
    public int getCount() {
        return listData == null ? 0 : listData.size();
    }

    @Override
    public Object getItem(int position) {
        if (listData != null && position >= 0 && position < listData.size()) {
            return listData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Component cpt;
        V viewHolder;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(getItemLayout(), null, false);
            viewHolder = getViewHolder(cpt);
            cpt.setTag(viewHolder);
        } else {
            cpt = convertComponent;
            viewHolder = (V) cpt.getTag();
        }
        convertItem(position, listData, viewHolder, convertComponent, componentContainer);
        return cpt;
    }

    protected abstract int getItemLayout();

    public abstract void convertItem(int position, List<T> data, V viewHolder, Component convertComponent, ComponentContainer componentContainer);

    public abstract V getViewHolder(Component component);

    public List<T> getListData() {
        return listData;
    }

    public int getScreenWidth(Context context) {
        return DisplayManager.getInstance().getDefaultDisplay(context).get().getRealAttributes().width;
    }
}

