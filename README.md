# SectionedRecyclerView

#### project Introduction
- SectionedRecyclerView is an adapter used to create a ListContainer with parts, providing headers and footers.
#### installation Tutorial

1. Add the library to your module build.gradle:

```
allprojects{
    repositories{
        mavenCentral()
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation 'com.gitee.archermind-ti:sectionedrecyclerview:1.0.1'
}
```

In sdk4, DevEco Studio2.1 beta4, the project can be run directly. If it fails to run, delete the project .gradle, .idea, build, gradle, build.gradle files, and create a new project based on your own version, and copy the corresponding files of the new project Go to the root directory; also pay attention to the location of the SDK in the local.properties file.
#### InstructionsForUse

In order to use this library, you need to extend `SectionedRecyclerView<H, VH, F>` where:

- `H` is a class extending `RecyclerView.ViewHolder` to hold the view for section **headers**.
- `VH` is a class extending `RecyclerView.ViewHolder` to hold the view for the regular **items** in the view.
- `F` is a class extending `RecyclerView.ViewHolder` to hold the view for section **footers**.
 
According to the sample published in this repository:

- 1. Create a class extending `SectionedRecyclerView`:

```java
public class CountSectionAdapter extends SectionedRecyclerViewAdapter<CountHeaderViewHolder,
        CountItemViewHolder,
        CountFooterViewHolder>
```

- 2. Implement the corresponding methods:

```java
@Override
protected int getItemCountForSection(int section) {
    return section + 1;
}


@Override
protected boolean hasFooterInSection(int section) {
    return true;
}

protected LayoutInflater getLayoutInflater(){
    return LayoutInflater.from(context);
}

@Override
protected CountHeaderViewHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
    View view = getLayoutInflater().inflate(R.layout.view_count_header, parent, false);
    return new CountHeaderViewHolder(view);
}

@Override
protected CountFooterViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
    View view = getLayoutInflater().inflate(R.layout.view_count_footer, parent, false);
    return new CountFooterViewHolder(view);
}

@Override
protected CountItemViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
    View view = getLayoutInflater().inflate(R.layout.view_count_item, parent, false);
    return new CountItemViewHolder(view);
}

@Override
protected void onBindSectionHeaderViewHolder(CountHeaderViewHolder holder, int section) {
    holder.render("Section " + (section + 1));
}

@Override
protected void onBindSectionFooterViewHolder(CountFooterViewHolder holder, int section) {
    holder.render("Footer " + (section + 1));
}

protected int[] colors = new int[]{0xfff44336, 0xff2196f3, 0xff009688, 0xff8bc34a, 0xffff9800};
@Override
protected void onBindItemViewHolder(CountItemViewHolder holder, int section, int position) {
    holder.render(String.valueOf(position + 1), colors[section]);
}
```


- 4. Your result will look like this:

![SectionedRecyclerView screenshot][1]
![SimpleSectionedAdapter screenshot][2]

## Even simpler

Most times you will need a simpler version of this adapter, where there are no footers and your headers will only be a title. For those cases, you have `SimpleSectionedAdapter<VH>`, where `VH` is a class extending `ViewHolder` to hold the view of the regular items in your `RecyclerView`.

In this case, you will have to implement the following methods:

```java
@Override
protected String getSectionHeaderTitle(int section) {
    return section == 0 ? "Today" : "Tomorrow";
}

@Override
protected int getItemCountForSection(int section) {
    return 3;
}

@Override
protected AgendaItemViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.view_agenda_item, parent, false);
    return new AgendaItemViewHolder(view);
}

protected String[][] agenda = {{"Meeting", "Phone call", "Interview"},
            {"Basket match", "Grocery shopping", "Taking a nap"}};

@Override
protected void onBindItemViewHolder(AgendaItemViewHolder holder, int section, int position) {
    holder.render(agenda[section][position]);
}
```

Your result will look like this:

![SimpleSectionedAdapter screenshot][3]

[1]: ./art/SOW-47_SectionedRecyclerView_g1.gif 
[2]: ./art/SOW-47_SectionedRecyclerView_g2.gif
[3]: ./art/SOW-47_SectionedRecyclerView_p1.jpg

#### versionIteration

- v1.2.0
- commit id 2f25149b8ae9ce87ddc451dbdf1d603d801b47b8

#### Copyright Licensing In formation
- Copyright 2015 Tomás Ruiz-López
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
     http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.